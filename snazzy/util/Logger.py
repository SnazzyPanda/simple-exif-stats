'''
Helper class for simple logging and other terminal output
@author: Snazzy Panda
'''

from datetime import datetime
from sys import stdout

class Logger(object):

	class COLOR:
		'''
		Class to hold escape codes for using color output in a terminal
		'''
		# colors can be found:
		# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux


		# Everything has the same start and end for this type of escaping
		START_ESCAPE = '\033['
		END_ESCAPE = 'm'

		# output colors
		BLACK = START_ESCAPE + '0;30' + END_ESCAPE
		BLUE = START_ESCAPE + '0;34' + END_ESCAPE
		# "Brown/Orange"
		BROWN = START_ESCAPE + '0;33' + END_ESCAPE
		CYAN = START_ESCAPE + '0;36' + END_ESCAPE
		DARK_GRAY = START_ESCAPE + '1;30' + END_ESCAPE
		GREEN = START_ESCAPE + '0;32' + END_ESCAPE

		LIGHT_BLUE = START_ESCAPE + '1;34' + END_ESCAPE
		LIGHT_CYAN = START_ESCAPE + '1;36' + END_ESCAPE
		LIGHT_GREEN = START_ESCAPE + '1;32' + END_ESCAPE

		LIGHT_GRAY = START_ESCAPE + '0;37' + END_ESCAPE
		LIGHT_PURPLE = START_ESCAPE + '1;35' + END_ESCAPE
		LIGHT_RED = START_ESCAPE + '1;31' + END_ESCAPE

		PURPLE = START_ESCAPE + '0;35' + END_ESCAPE
		RED = START_ESCAPE + '0;31' + END_ESCAPE
		WHITE = START_ESCAPE + ';37' + END_ESCAPE
		YELLOW = START_ESCAPE + '1;33' + END_ESCAPE

		# No color (reset to default behavior?)
		NC = START_ESCAPE + '0' + END_ESCAPE
		#NC = '\033[0m'
	# end inner COLOR class



	DEBUG_LABEL = "[DEBUG]"
	INFO_LABEL = "[INFO]"
	WARNING_LABEL = "[WARN]"
	ERROR_LABEL = "[ERROR]"

	NONE_COLOR = COLOR.NC
	DEBUG_COLOR = COLOR.CYAN
	INFO_COLOR = COLOR.WHITE
	WARNING_COLOR = COLOR.YELLOW
	ERROR_COLOR = COLOR.RED

	COLOR_CLEARING_ESCAPE = COLOR.NC


	def __init__(self, colorize = True):
		'''
		Constructor
		'''
		# ensure stdout is tty before allowing colors, as it apparently could cause problems otherwise
		#(https://stackoverflow.com/questions/2330245/python-change-text-color-in-shell)
		if(not stdout.isatty()):
			colorize = False
		# end if stdout is not tty

		self.colorize = colorize
	# end constructor

	def print(self):
		print(str(message))
	# end print

	def empty(self, message):
		self.print(message)
	# end empty

	def none(self, message):
		output = self.getNonePrefix() + ' ' + str(message) + self.getPostfix()
	# end none

	def debug(self, message):
		output = self.getDebugPrefix() + ' ' + str(message) + self.getPostfix()
		print(output)
	# end debug

	def info(self, message):
		output = self.getInfoPrefix() + ' ' + str(message) + self.getPostfix()
		print(output)
	# end info

	def warn(self, message):
		output = self.getWarningPrefix() + ' ' + str(message) + self.getPostfix()
		print(output)
	# end warn

	def error(self, message):
		output = self.getErrorPrefix() + ' ' + str(message) + self.getPostfix()
		print(output)
	# end error

	def getNonePrefix(self):
		'''
		Prefix containing no initial tag, only a timestamp
		'''
		prefix = '';
		if(self.colorize):
			prefix += self.NONE_COLOR
		# end if output should be in color

		prefix += self.getTimestamp()
		return prefix
	#end getNonePrefix

	def getDebugPrefix(self):
		prefix = '';
		if(self.colorize):
			prefix += self.DEBUG_COLOR
		# end if output should be in color

		prefix += self.DEBUG_LABEL + self.getTimestamp()
		return prefix
	# end getDebugPrefix

	def getInfoPrefix(self):
		prefix = '';
		if(self.colorize):
			prefix += self.INFO_COLOR
		# end if output should be in color

		prefix += self.INFO_LABEL + self.getTimestamp()
		return prefix
	# end getInfoPrefix

	def getWarningPrefix(self):
		prefix = '';
		if(self.colorize):
			prefix += self.WARNING_COLOR
		# end if output should be in color

		prefix += self.WARNING_LABEL + self.getTimestamp()
		return prefix
	# end getWarningPrefix


	def getErrorPrefix(self):
		prefix = '';
		if(self.colorize):
			prefix += self.ERROR_COLOR
		# end if output should be in color

		prefix += self.ERROR_LABEL + self.getTimestamp()
		return prefix
	# end getErrorPrefix

	def getPostfix(self):
		postfix = ''
		if(self.colorize):
			postfix += self.COLOR_CLEARING_ESCAPE
		# end if use color
		return postfix
	# end getPostfix

	def getTimestamp(self):
		return '[' + '{:%H:%M:%S}'.format(datetime.now()) + ']'
	# end getTimestamp

	def getDateTimestamp(self):
		return '[' + '{:%Y-%m-%d %H:%M:%S}'.format(datetime.now()) + ']'
	# end getDateTimestamp

# end Logger
