

from PIL.ExifTags import TAGS

from snazzy.util.Logger import Logger

class ExifDataParser(object):

	logger = Logger()

	def parse(self, key, value):
		if(key == 'FocalLength'):
			return self.parseFocalLength(value)
		elif(key == 'ExposureTime'):
			return self.parseExposureTime(value)
		elif(key == 'FNumber'):
			return self.parseFNumber(value)
		elif(key == 'Orientation'):
			return self.parseOrientation(value)
		else:
			return value

	def parseFocalLength(self, value, decimal = True):
		'''
		(this seems to be a list of 2 values)
		FocalLength seems like the mm value is found with first element divided by second element
		'''
		if(decimal):
			return round(float(value[0]) / float(value[1]), 1)
		else:
			return str(value[0]) + '/' + str(value[1])
	# end FocalLength

	def parseExposureTime(self, value, decimal = False):
		'''
		(this seems to be a list of 2 values)
		ExposureTime seems to be first element divided by second element is the time in seconds
		'''
		if(decimal):
			return float(value[0]) / float(value[1])
		else:
			return str(value[0]) + '/' + str(value[1])
	# end ExposureTime

	def parseFNumber(self, value):
		'''
		frankly, taking a guess that it is first value divided by second...
		'''
		return float(value[0]) / float(value[1])
		#return value
		pass
	# end FNumber

	def parseOrientation(self, value):
		'''
		taking guesses/making assumptions that 1 means landscape orientation and anything else means portrait
		'''
		if(value == 1):
			return 'Landscape'
		else:
			return 'Portrait'
	# end Orientation


