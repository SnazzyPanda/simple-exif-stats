'''


@author: Snazzy Panda
'''


import argparse
import sys

# trying to sort the dict
import collections

from PIL import Image
from PIL.ExifTags import TAGS

from snazzy.helpers.IOHelper import IOHelper
from snazzy.util.Logger import Logger

from snazzy.image.exif_parse import ExifDataParser

BLACKLIST_TAGS = [
	'ComponentsConfiguration',
	'MakerNote',
	'PrintImageMatching',
	'UserComment',
	'ImageDescription',
	'FileSource',
	'SceneType',
	'ExifVersion',
	'FlashPixVersion'
]

WHITELIST_TAGS = [
	'FocalLength',
	'Make',
	'Model',
	'Orientation',
	'ExposureTime',
	'FNumber',
	'ISOSpeedRatings',
	'Software',
	'LensModel',
	'Flash'
]

def get_exif(fn):
	edf = ExifDataParser()

	ret = {}
	i = Image.open(fn)
	info = i._getexif()
	for tag, value in info.items():
		decoded = TAGS.get(tag, tag)
		#if(decoded not in BLACKLIST_TAGS):
		if(decoded in WHITELIST_TAGS):
			ret[decoded] = edf.parse(decoded, value)
		# end if
	# end for
	i.close()
	return ret
#

def getUncheckedExifTags(fn):

	edf = ExifDataParser()

	ret = []
	i = Image.open(fn)
	info = i._getexif()
	for tag, value in info.items():
		decoded = TAGS.get(tag, tag)
		if(decoded not in WHITELIST_TAGS and decoded not in BLACKLIST_TAGS):
			ret += [decoded]
		# end if
	# end for
	i.close()
	return ret
#


def getExifInfo(image):
	# does not currently work right
	exif = dict()
	edf = ExifDataParser()

	exifdata = image._getexif()

	for (tag, value) in exifdata.items():
		decoded = TAGS.get(tag, tag)
		#if(decoded not in BLACKLIST_TAGS):
		if(decoded in WHITELIST_TAGS):
			exif[decoded] = edf.parse(decoded, value)

	return exif
# end

def sortExposureFraction(s):
	'''
	Fractions do not sort as desired by default, this will make the fractions for ExposureTime sort as one would expect
	'''
	n,d = s[0].split('/')
	decimal = int(n) / int(d)
	return decimal
#

if __name__ == '__main__':
	logger = Logger()
	iohelper = IOHelper()

	logger.info(str(sys.argv))
	startingDir = None
	if(len(sys.argv) > 1 and len(sys.argv[1]) > 0):
		startingDir = sys.argv[1]
	#


	if(startingDir is None):
		startingDir = input("Enter starting directory: \n")
	#

	logger.debug(str(startingDir))
	# TODO: handle allowing user to specify just given path or all child paths as well
	innerDirs = iohelper.getDeepDirsInPath(startingDir)
	#innerDirs = [startingDir]

	allFiles = list()

	for directory in innerDirs:
		allFiles = allFiles + iohelper.getFilesInPath(directory, ['jpg', 'jpeg'])
	#


	unchecked = []
	if(len(allFiles) > 0):
		unchecked = getUncheckedExifTags(allFiles[0])

	exifstats = dict()
	exiflist = list()

	totalnumber = 0

	for img in allFiles:

		exiflist += [get_exif(img)]
		totalnumber += 1

		#with Image.open(img) as tmpimage:
		#	exiflist += getExifInfo(tmpimage)
		#	# do stuff
		#	#exifstats += tmpimage.getexif()
		#	pass
		# end with
		#img = PIL.Image.open('img.jpg')
		#exif_data = img._getexif()
	# end foreach allFiles

	for exifdict in exiflist:
		for key, value in exifdict.items():
			if(key not in exifstats):
				exifstats[key] = {value: 0}
			if(value not in exifstats[key]):
				exifstats[key][value] = 0
			exifstats[key][value] += 1
			pass
		#
	#


	# sort the dict
	exifstats = collections.OrderedDict(sorted(exifstats.items()))


	# OUTPUT STATS
	for attr, valdict in exifstats.items():
		# sort the dict

		if(attr == 'ExposureTime'):
			valdict = collections.OrderedDict(sorted(valdict.items(), key=sortExposureFraction, reverse=True))
		else:
			valdict = collections.OrderedDict(sorted(valdict.items()))
		#

		print(logger.COLOR.LIGHT_PURPLE + str(attr) + logger.COLOR.NC)
		for (value, count) in valdict.items():
			percent = round((int(count) / totalnumber) * 100, 2)
			print(str(value) + ":\t\t" + str(count) + "\t\t(" + str(percent) + "%)")
		#
	#

	print(logger.COLOR.LIGHT_PURPLE + "Total Images Checked" + logger.COLOR.NC)
	print(totalnumber)



	logger.warn("The following Exif data was not handled: " + str(unchecked))
	# TODO: sort/format the output of exif stats (can something do a table for me?)
	logger.warn('TODO: sort the output exif stats...')

# end if __name__ == '__main__':



